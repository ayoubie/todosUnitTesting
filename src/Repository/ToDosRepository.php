<?php

namespace App\Repository;

use App\Entity\ToDos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ToDos|null find($id, $lockMode = null, $lockVersion = null)
 * @method ToDos|null findOneBy(array $criteria, array $orderBy = null)
 * @method ToDos[]    findAll()
 * @method ToDos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ToDosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ToDos::class);
    }

    // /**
    //  * @return ToDos[] Returns an array of ToDos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ToDos
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
