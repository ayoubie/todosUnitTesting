<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $birthday;

    public $errors = Array();

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ToDos", cascade={"persist", "remove"})
     */
    private $todolist;

    /**
     * User constructor.
     * @param $firstname
     * @param $lastname
     * @param $email
     * @param $password
     * @param $birthday
     */
    public function __construct($firstname, $lastname, $email, $password, $birthday)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->password = $password;
        $this->birthday = $birthday;
        $this->todolist = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    public function setBirthday(string $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }


    private function isEmail(){
        if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)){
            $this->errors[] = "Email not valid";
        }
    }

    private function isNotEmpty($var, $type){
        if(!strlen($var) > 0)
            $this->errors[] = $type." is empty";
    }

    private function isPassword(){
        if(!(strlen($this->password) >=8 && strlen($this->password) <= 40) ){
            $this->errors[] = "Password not valid, it must be between 8 and 40 characters";
        }
    }

    private function isValidBirthday(){
        $date = explode('/', $this->birthday);
        if(count($date) !== 3){
            $this->errors[] = "Date not valid (DD/MM/YYYY)";
        }

        $age = (date('Y') - $date[2]);
        if(($date[1] - date('m')) > 0){
            $age = ($age - 1);
        }
        if(($date[1] - date('m')) == 0 && ($date[0] - date('d')) > 0){
            $age = ($age - 1);
        }
        if($age < 13){
            $this->errors[] = "You must be over 13 years of age";
        }
    }

    public function isValide(){
        $this->isEmail();
        $this->isNotEmpty($this->firstname, 'firstaname');
        $this->isNotEmpty($this->lastname, 'lastname');
        $this->isPassword();
        $this->isValidBirthday();

        if(count($this->errors) > 0){
            var_dump($this->errors);
            return false;
        }
        return true;
    }

    public function hasOneToDoList(){
        if(count($this->todolist)>1)
            return false;

        return true;
    }

    public function getTodolist(): ?ToDos
    {
        return $this->todolist;
    }

    public function setTodolist(?ToDos $todolist): self
    {
        $this->todolist = $todolist;

        return $this;
    }
}
