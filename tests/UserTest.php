<?php


namespace App\Tests;
namespace App\Entity;

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $user;

    public function __construct()
    {
        parent::__construct();
        $this->user = new User('ayoub', 'ibnc', 'ayoub.ibnc@gmail.com', '12345678','04/04/1996');
    }

    public function testUserValidation(){
        $this->assertTrue($this->user->isValide());
    }

    public function testUserHasOneToDos(){
        $this->assertTrue($this->user->hasOneToDoList());
    }
}